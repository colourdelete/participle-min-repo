package main

import (
	"fmt"
	"gitlab.com/colourdelete/participle-min-repo/parser"
	"os"
)

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	ast, err := parser.Parse(file)
	if err != nil {
		panic(err)
	}
	fmt.Println(ast)
}
