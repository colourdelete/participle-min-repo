module gitlab.com/colourdelete/participle-min-repo

go 1.15

require (
	github.com/alecthomas/participle v0.6.0
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1
	github.com/gookit/color v1.3.1
	github.com/wayneashleyberry/terminal-dimensions v1.0.0
)
