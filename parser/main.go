package parser

import (
	"github.com/alecthomas/participle"
	"github.com/alecthomas/participle/lexer/stateful"
	"github.com/alecthomas/repr"
	"io"
	"log"
	"strings"
)

func Parse(src io.Reader) (*RawBlock, error) {
	def, err := stateful.New(stateful.Rules{
		"Root": {
			{`RawBlock`, `[^{]`, stateful.Push("RawBlock")},
		},
		"Block": {
			{"OBrack", `\[`, nil},
			{"CBrack", `\]`, stateful.Pop()},
		},
		"Obj": {
			stateful.Include("Root"),
			{`whitespace`, `\s+`, nil},
			{`Oper`, `[-+/*%]`, nil},
			{"Ident", `\w+`, nil},
			{"End", `}`, stateful.Pop()},
		},
		"Map": {
			{"OParen", `\(`, nil},
			{"CParen", `\)`, stateful.Pop()},
		},
		"Text": {
			{"OSQuote", `\(`, nil},
			{"In", `"(\\.|[^"\\])*"`, nil},
			{"CSQuote", `\)`, stateful.Pop()},
		},
		"Ref": {
			{"NonDot", `[^\.]`, nil},
			{"Dot", `\.`, nil},
		},
	})
	if err != nil {
		log.Fatal(err)
	}
	parser, err := participle.Build(&RawBlock{}, participle.Lexer(def))
	if err != nil {
		log.Fatal(err)
	}

	actual := &RawBlock{}
	buf := new(strings.Builder)
	_, err = io.Copy(buf, src)
	if err != nil {
		log.Fatal(err)
	}
	err = parser.ParseString(buf.String(), actual)
	if err != nil {
		log.Fatal(err)
	}
	repr.Println(actual)
	return nil, nil
}
